package org.kangaroo.rim.device.actions;

public enum CommandType {
    
    CLICK, HOLD, RELEASE
    
}
