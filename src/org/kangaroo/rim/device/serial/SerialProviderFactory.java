package org.kangaroo.rim.device.serial;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.kangaroo.rim.device.serial.providers.BluetoothMasterProvider;
import org.kangaroo.rim.device.serial.providers.BluetoothSlaveProvider;
import org.kangaroo.rim.device.serial.providers.SerialProvider;
import org.kangaroo.rim.device.serial.providers.USBProvider;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.Parcelable;

@EBean
public class SerialProviderFactory {

    @RootContext Context context;

    public SerialProvider getProvider(Parcelable device, boolean master) {

        if (device != null) {
            if (BluetoothDevice.class.equals(device.getClass())) {
                if (master) {
                    return new BluetoothMasterProvider(context, (BluetoothDevice) device);
                } else {
                    return new BluetoothSlaveProvider(context, (BluetoothDevice) device);
                }
            }
            if (UsbDevice.class.equals(device.getClass())) {
                return new USBProvider(context, (UsbDevice) device);
            }
        }

        return null;
    }

}
