package org.kangaroo.rim;

import java.util.LinkedList;
import java.util.List;

import lombok.val;

import org.androidannotations.annotations.EApplication;
import org.androidannotations.annotations.SystemService;
import org.kangaroo.rim.activity.UsbStateActivity_;
import org.kangaroo.rim.device.entitiy.AppInfo;
import org.kangaroo.rim.device.entitiy.TaskInfo;
import org.kangaroo.rim.service.SerialListenerService_;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Parcelable;
import android.preference.PreferenceManager;

@EApplication
public class RemoteInputsMgr extends Application {

    public static final String PRIVATE_PERMISSION = "org.kangaroo.rim.permission.PRIVATE";

    public static final String ACTION_CONNECT = "org.kangaroo.rim.action.ACTION_CONNECT";
    public static final String ACTION_SETUP = "org.kangaroo.rim.action.ACTION_SETUP";

    public static final String ACTION_DATA_RECEIVE = "org.kangaroo.rim.action.ACTION_DATA_RECEIVE";
    public static final String ACTION_DATA_SEND = "org.kangaroo.rim.action.ACTION_DATA_SEND";

    public static final String EXTRA_DEVICE = "org.kangaroo.rim.serial.DEVICE";
    public static final String EXTRA_MODE = "org.kangaroo.rim.serial.MODE";
    public static final String EXTRA_BAUDRATE = "org.kangaroo.rim.serial.BAUDRATE";

    public static final String EXTRA_COMMAND = "org.kangaroo.rim.device.EXTRA_COMMAND";
    public static final String EXTRA_ARGS = "org.kangaroo.rim.device.EXTRA_ARGS";

    @SystemService UsbManager usbManager;

    private List<AppInfo> packages;
    private List<TaskInfo> tasks;

    @Override
    public void onCreate() {

        super.onCreate();

        SerialListenerService_.intent(this)
                .extra(EXTRA_DEVICE, getDevice())
                .extra(EXTRA_MODE, getConnectionMode())
                .extra(EXTRA_BAUDRATE, getBaudrate())
                .start();

    }

    public List<AppInfo> getPackages() {

        if (packages == null) {
            getPackagesForce();
        }

        return packages;
    }

    public List<TaskInfo> getTasks() {

        if (tasks == null) {
            getTasksForce();
        }

        return tasks;
    }

    public List<AppInfo> getPackagesForce() {

        packages = new LinkedList<AppInfo>();
        for (PackageInfo pi : getPackageManager().getInstalledPackages(0)) {
            packages.add(new AppInfo(this, pi));
        }

        return packages;
    }

    public List<TaskInfo> getTasksForce() {

        tasks = new LinkedList<TaskInfo>();
        Cursor c = getContentResolver().query(Uri.parse("content://net.dinglisch.android.tasker/tasks"), null, null, null, null);
        if (c != null) {
            int projectCol = c.getColumnIndex("project_name");
            int nameCol = c.getColumnIndex("name");
            while (c.moveToNext()) {
                tasks.add(new TaskInfo(c.getString(projectCol), c.getString(nameCol)));
            }
            c.close();
        }

        return tasks;
    }

    public void refreshConnection() {

        val intent = new Intent(ACTION_CONNECT);

        intent.putExtra(EXTRA_DEVICE, getDevice());
        intent.putExtra(EXTRA_MODE, getConnectionMode());
        intent.putExtra(EXTRA_BAUDRATE, getBaudrate());

        sendBroadcast(intent, RemoteInputsMgr.PRIVATE_PERMISSION);
    }

    private SharedPreferences getSharedPreferences() {

        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    private String getConnectionType() {

        return getSharedPreferences().getString("connection_type", "Bluetooth");
    }

    private String getConnectionMode() {

        return getSharedPreferences().getString("connection_mode", "Slave");
    }

    private String getDeviceAddress() {

        return getSharedPreferences().getString("device_id", "-1");
    }

    private Parcelable getDevice() {

        if ("-1".equals(getDeviceAddress())) {
            return null;
        }

        if ("Bluetooth".equals(getConnectionType())) {
            return BluetoothAdapter.getDefaultAdapter().getRemoteDevice(getDeviceAddress());
        } else {
            return getDevice(this, Integer.valueOf(getDeviceAddress()));
        }
    }

    private int getBaudrate() {

        return Integer.valueOf(getSharedPreferences().getString("device_baudrate", "9600"));
    }

    private UsbDevice getDevice(Context context, int deviceId) {

        for (UsbDevice device : usbManager.getDeviceList().values()) {
            if (device.getDeviceId() == deviceId) {
                if (!usbManager.hasPermission(device)) {
                    UsbStateActivity_.intent(context)
                            .extra(UsbManager.EXTRA_DEVICE, device)
                            .flags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .start();
                    return null;
                }
                return device;
            }
        }
        return null;
    }

}
